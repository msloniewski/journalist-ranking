<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('journalist_id');
            $table->foreign('journalist_id')
                ->references('id')
                ->on('journalist')
                ->onDelete('cascade');

            $table->unsignedInteger('rating');

            $table->string('vote_ip', 16);
            $table->unique(['journalist_id', 'vote_ip']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote');
    }
}
