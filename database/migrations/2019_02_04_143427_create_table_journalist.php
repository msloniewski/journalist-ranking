<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJournalist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journalist', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name', 128);
            $table->string('second_name', 128);
            $table->string('slug', 128);
            $table->text('description');
            $table->string('image', 128);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journalist');
    }
}
