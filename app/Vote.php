<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $table = 'vote';
    protected $fillable = [
        'journalist_id',
        'rating',
        'vote_ip'
    ];

    public function journalist()
    {
        return $this->belongsTo('App\Journalist');
    }

}
