<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class JournalistCollection extends ResourceCollection
{

    public $collects = 'App\Http\Resources\JournalistResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'data' => $this->collection,
          'self' => $request->getUri(),
          'next' => '',
          'prev' => '',
          'image_location' => 'images'
        ];
    }
}
