<?php

namespace App\Http\Requests;

use App\Vote;
use Illuminate\Foundation\Http\FormRequest;
use App\Services\SettingsService;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;


class VoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(SettingsService $service)
    {
        $count = Vote::where('vote_ip','=', $this->getClientIp())
            ->count('vote_ip');
        if ($count >= $service->get('max_votes')) {
            return false;
        }
        return true;
    }

    protected function failedAuthorization()
    {
        throw new HttpResponseException($this->forbiddenResponse());
    }

    public function forbiddenResponse()
    {
        return new JsonResponse(['message'=> 'vote limit exceeded'], 403);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate' => 'required|integer'
        ];
    }
}
