<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JournalistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:128|string',
            'second_name' => 'required|max:128|string',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Test custom message',
        ];
    }
}
