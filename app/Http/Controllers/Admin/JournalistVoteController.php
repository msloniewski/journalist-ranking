<?php

namespace App\Http\Controllers\Admin;

use App\Journalist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;




class JournalistVoteController extends Controller
{
    public function index(Request $request, Journalist $journalist)
    {
        return view('admin.vote.index', [
            'object_list' => $journalist->votes()->paginate(15),
            'journalist' => $journalist
        ]);
    }
}
