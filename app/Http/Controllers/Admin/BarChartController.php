<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Journalist;

class BarChartController extends Controller
{
    public function show()
    {
        return view('admin/chart/bar-chart');
    }

    public function data()
    {
        $journalists = Journalist::with('votes')->paginate(10);

        $labels = [];
        $data = [];
        foreach ($journalists as $journalist) {
            $labels[] = $journalist->fullName();
            $data[] = $journalist->votes->average('rating');
        }

        $reponse_data = [
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Jouralists',
                    'data' => $data
                ],
            ]
        ];
        return new JsonResponse($reponse_data);
    }
}
