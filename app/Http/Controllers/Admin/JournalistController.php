<?php

namespace App\Http\Controllers\Admin;

use App\Journalist;
use App\Http\Requests\JournalistRequest;
use App\Http\Controllers\Controller;

class JournalistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.journalist.index', [
            'journalist_list' => Journalist::with('votes')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.journalist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\JournalistRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JournalistRequest $request)
    {
        $journalist = Journalist::create(
                $request->only(['first_name', 'second_name','description'])
                + ['image' => $this->processImage($request)]
        );
        $journalist->save();

        return redirect()->route('admin.journalist.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function show(Journalist $journalist)
    {
        return view('admin.journalist.show', compact('journalist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function edit(Journalist $journalist)
    {
        return view('admin/journalist/update', compact('journalist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function update(JournalistRequest $request, Journalist $journalist)
    {
        $journalist->update(array_merge(
            $request->only(['first_name', 'second_name','description'])
            + ['image' => $this->processImage($request)]
        ));
        $journalist->save();
        return redirect()
            ->route('admin.journalist.show', ['journalist' => $journalist->slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journalist $journalist)
    {
        $journalist->delete();
        return redirect()->route('admin.journalist.index');
    }

    /**
     * Process the submitted image
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string $image_name
     */

    private function processImage($request)
    {
        if($request->file('image') != null) {
            $image_name = implode(
                    '.',
                    [
                        $request->first_name,
                        $request->second_name,
                        $request->file('image')->getClientOriginalExtension()
                    ]
                );
            $request->file('image')->move('images', $image_name);
        } else {
            $image_name = 'default.png';
        }
        return $image_name;
    }
}
