<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingsRequest;
use App\Http\Controllers\Controller;
use App\Services\SettingsService;
use App\Settings;

class SettingsController extends Controller
{

    public function index(SettingsService $service)
    {
        return view('admin.settings.index',
            [ 'settings' => $service->get_all() ]
        );
    }

    public function update(SettingsRequest $request, SettingsService $service)
    {
        $settings = Settings::firstOrNew(['key' => $request->setting]);
        $settings->value = $request->value;
        $settings->save();

        return view('admin.settings.index',
            ['settings' => $service->get_all()]
        );
    }
}
