<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Vote;

class VoteController extends Controller
{
    public function destroy(Request $request, Vote $vote)
    {
        $slug = $vote->journalist->slug;
        $vote->delete();
        return redirect()->route('admin.journalist.votes', ['journalist' => $slug]);
    }
}
