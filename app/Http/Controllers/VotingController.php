<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

use App\Http\Requests\VoteRequest;
use App\Vote;
use App\Journalist;


class VotingController extends Controller
{
    public function vote(VoteRequest $request, Journalist $journalist)
    {
        $vote = Vote::firstOrNew([
            'journalist_id'=> $journalist->id,
            'vote_ip' => $request->getClientIp()
        ]);
        $vote->rating = $request->rate;
        $vote->save();

        return new JsonResponse(
            [
                'message' => 'thank you for voting',
                'avg_rate' => $journalist->votes->average('rating'),
                'slug' => $journalist->slug
            ],
            201);
    }
}

