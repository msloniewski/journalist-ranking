<?php

namespace App\Http\Controllers;

use App\Journalist;


class JournalistController extends Controller
{
    public function index()
    {
        return view('webpage.journalist.index',
            ['journalist_list' => Journalist::with('votes')->paginate(10)]);
    }
}
