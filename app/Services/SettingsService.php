<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;
use App\Settings;
use App\Vote;


class SettingsService
{
    private $settings = [
        'max_votes'
    ];

    public function get_all()
    {
        $result = [];
        foreach($this->settings as $key) {
            $result[$key] = $this->get($key);
        };
        return $result;
    }

    public function get($key)
    {
        $setting = Settings::where('key', $key)->first();
        if (is_null($setting)) {
            $value = Config::get('custom.'.$key);
        } else {
            $value = $setting->value;
        }
        return $value;
    }
}
