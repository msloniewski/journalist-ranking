<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journalist extends Model
{
    protected $table = 'journalist';
    protected $fillable = [
        'first_name',
        'second_name',
        'image',
        'description',
        'slug',
    ];

    public function save(array $options = [])
    {
        if ($this->slug == null)
        {
            $this->slug = str_slug(
                implode(' ',[$this->first_name, $this->second_name])
            );
        }
        return parent::save($options);
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function fullName()
    {
        return $this->first_name.' '.$this->second_name;
    }
}
