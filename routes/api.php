<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Journalist;
use App\Http\Resources\JournalistCollection;

Route::get('journalist', function () {
    return new JournalistCollection(Journalist::all());
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
