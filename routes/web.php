<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Webpage routes
Route::get('/', 'JournalistController@index')->name('journalist-list');
Route::post('journalist/{journalist}/vote/', 'VotingController@vote')->name('journalist-vote');

// Admin routes
Route::prefix('admin')
    ->name('admin.')
    ->namespace('Admin')
    ->middleware('auth')
    ->group(function () {
        Route::resource('settings', 'SettingsController', ['only' => ['index', 'update']]);
        Route::resource('vote', 'VoteController', ['only' => ['destroy']]);
        Route::resource('journalist', 'JournalistController');

        Route::get('journalist/{journalist}/votes', 'JournalistVoteController@index')
            ->name('journalist.votes');

        Route::get('/', function(){
            return view('admin/admin-main');
        })->name('main');

        Route::get('/charts/bar-chart', 'BarChartController@show')
            ->name('bar-chart');
        Route::post('/charts/bar-chart', 'BarChartController@data')
            ->name('bar-chart-data');
});

// Auth routes
Route::namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::namespace('Auth')
    ->prefix('password')
    ->name('password.')
    ->group(function () {
        Route::get('reset', 'ForgotPasswordController@showLinkRequestForm')->name('request');
        Route::post('email', 'ForgotPasswordController@sendResetLinkEmail')->name('email');

        Route::get('reset/{token}', 'ResetPasswordController@showResetForm')->name('reset');
        Route::post('reset', 'ResetPasswordController@reset')->name('update');
});
