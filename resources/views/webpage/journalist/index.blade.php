@extends('webpage/layout')

@section('content')
<div class="row" style="margin-top:4rem;">
    @foreach ($journalist_list as $journalist)
    <div class="col-md-3">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="/images/{{ $journalist->image }}" alt="{{ $journalist->first_name }} {{ $journalist->second_name }} picture">
            <div class="card-body">
                <h5 class="card-title">{{ $journalist->first_name }} {{ $journalist->second_name }}</h5>
                <p class="card-text">{{ $journalist->description }}</p>
                <p class="card-text">Vote average:
                    <span id="rate-{{ $journalist->slug }}" class="rate_box">
                        {{ $journalist->votes->average('rating') }}
                    </span>
                </p>
                <div class="btn-toolbar">
                    <div class="btn-group mr-2">
                        <a href="{{ route('journalist-vote', ['journalist' => $journalist->slug ], false) }}"class="btn btn-secondary vote" data-rating="1">1</a>
                        <a href="{{ route('journalist-vote', ['journalist' => $journalist->slug ], false) }}"class="btn btn-secondary vote" data-rating="2">2</a>
                        <a href="{{ route('journalist-vote', ['journalist' => $journalist->slug ], false) }}"class="btn btn-secondary vote" data-rating="3">3</a>
                        <a href="{{ route('journalist-vote', ['journalist' => $journalist->slug ], false) }}"class="btn btn-secondary vote" data-rating="4">4</a>
                        <a href="{{ route('journalist-vote', ['journalist' => $journalist->slug ], false) }}"class="btn btn-secondary vote" data-rating="5">5</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

    <div class="col-md-12">
        {{ $journalist_list->links() }}
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var votes = $('.vote');
    votes.on('click', function(event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var rate = $(this).data('rating');
        $.ajax({
            url: href,
            method: 'POST',
            data: {
                rate: rate
            },
            statusCode: {
                201: function (data) {
                    console.log(data.message)
                    $("#rate-" + data.slug).html(data.avg_rate);
                },
                403: function (data) {
                    console.log('error: ' + data.responseJSON.message)
                },
                404: function (data) {
                    console.log('error: url not found')
                },
                419: function(data) {
                    console.log('error: csrf check failed')
                }
            }
        }).done(function (data) {
            console.log('vote success')
        }).fail(function() {
            console.log('ajax call failed')
        });
    })
</script>
@endsection
