@extends('admin.layout')

@section('content')
<section class="content-header">

</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Votes for {{ $journalist->first_name }} {{ $journalist->second_name }}</h3>
                    </div>
                <ul class="list-group">
                    @foreach($object_list as $object)
                    <li class="list-group-item">
                        <div class="form-group">
                            <form action="{{ route('admin.vote.destroy', ['id' => $object->id ], False) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <input type="submit" name="delete" value="delete" class="btn btn-danger btn-sm pull-right">
                            </form>
                            {{ $object->vote_ip }} at {{ $object->created_at }}
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            {{ $object_list->links() }}
        </div>
    </div>
</section>
@endsection
