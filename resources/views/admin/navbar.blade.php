<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <i class="fa fa-circle-o text-green"></i>
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu tree" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Charts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ route('admin.bar-chart') }}"><i class="fa fa-circle-o"></i> Ranking</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Journalists</span>
          </a>
          <ul class="treeview-menu">
              <li>
                  <a href="{{ route( 'admin.journalist.index' ) }}"><i class="fa fa-circle-o"></i>List</a>
              </li>
              <li>
                  <a href="{{ route( 'admin.journalist.create' ) }}"><i class="fa fa-circle-o"></i>Create</a>
              </li>
          </ul>
        </li>
        <li class="header">Application</li>
        <li>
            <a href="{{ route( 'admin.settings.index' ) }}"><i class="fa fa-circle-o text-aqua"></i><span>Settings</span></a>
        </li>
  </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
