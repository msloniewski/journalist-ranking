@extends('admin/layout')


@section('content')
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Bar Chart</h3>
        </div>
        <div class="box-body">
            <div class="chart">
                <canvas id="barChart" style="height: 230px; width: 788px;" width="788" height="230"></canvas>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '{{ route('admin.bar-chart-data', [], false) }}',
        method: 'POST',
        statusCode: {
            200: function (data) {
                var barChartCanvas = $('#barChart').get(0).getContext('2d');
                var barChart = new Chart(barChartCanvas);
                barChart.Bar(data);
                console.log(data)
            },
        }
    }).done(function (data) {
        console.log('success')
    }).fail(function() {
        console.log('failed')
    });
</script>
@endsection
