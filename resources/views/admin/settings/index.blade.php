@extends('admin/layout')

@section('title')
    Settings
@endsection


@section('content')
<section class="content">
    <div class="row">
        @if (isset($message))
        <div class="col-md-6">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success! Settings saved</h4>
            </div>
        </div>
        @endif
    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Custom settings</h3>
                </div>
                <div class="box-body">
                    @foreach($settings as $key => $value)
                    <form class="" action="{{ route('admin.settings.update', ['setting' => $key ], False)  }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="form-group  {{ $errors->$key->has('value') ? 'has-error' : '' }}">
                            <label for="first_name">max_votes</label>
                            <input type="text" name="value" class="form-control" value="{{ $value}}">
                            <span class="help-block">{{ $errors->$key->first('value') }}</span>
                        </div>
                        <input type="submit" name="" value="Save" class="btn btn-success">
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
