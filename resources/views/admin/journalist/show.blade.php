@extends('admin.layout')

@section('content')
<section class="content-header">
      <h1>
        Journalist details
      </h1>
</section>
<section class="content">
    <div class="box box-widget widget-user">
        <div class="widget-user-header bg-aqua-active">
            <h3 class="widget-user-username">{{ $journalist->first_name }} {{ $journalist->second_name }}</h3>
            <h5 class="widget-user-desc">{{ $journalist->description }}</h5>
        </div>
        <div class="widget-user-image">
            <img class="img-circle" src="/images/{{ $journalist->image }}" alt="User Avatar">
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-6 border-right">
                    <div class="description-block">
                        <h5 class="description-header">{{ $journalist->votes->average('rating') }}</h5>
                        <span class="description-text">vote average</span>
                    </div>
                </div>

                <div class="col-sm-6 border-right">
                    <div class="description-block">
                        <h5 class="description-header">{{ $journalist->votes->count() }}</h5>
                        <span class="description-text">vote count</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</secion>
@endsection
