@extends('admin.layout')

@section('content')
<section class="content-header">
      <h1>
        Journalist list
        <small>a list of journalists</small>
      </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Journalist table</h3>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>First name</th>
                                <th>Second name</th>
                                <th>Votes</th>
                                <th>Rating</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($journalist_list as $journalist)
                            <tr>
                                <td>{{ $journalist->id }}</td>
                                <td>{{ $journalist->first_name }}</td>
                                <td>{{ $journalist->second_name }}</td>
                                <td>{{ $journalist->votes->count() }}</td>
                                <td>{{ $journalist->votes->average('rating') }}</td>
                                <td>
                                  <a href="{{ route('admin.journalist.show',['journalist' => $journalist->slug] , False) }}">
                                      Details</a> |
                                  <a href="{{ route('admin.journalist.edit',['journalist' => $journalist->slug] , False) }}">
                                      Edit</a> |
                                  <a href="{{ route('admin.journalist.votes',['journalist' => $journalist->slug] , False) }}">
                                      Votes</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
              <!-- /.box -->
        </div>
        <div class="col-md-12">
            {{ $journalist_list->links() }}
        </div>
    </div>
</section>
@endsection
