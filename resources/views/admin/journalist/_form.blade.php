<div class="box-body">
    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
        <label for="first_name">Name</label>
        <input type="text" name="first_name" class="form-control" value="{{ isset($journalist) ? $journalist->first_name : old('first_name') }}">
        @if ( $errors->has('first_name') )
            <span class="help-block">{{ $errors->first('first_name') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('second_name') ? 'has-error' : '' }}">
        <label for="second_name">Surname</label>
        <input type="text" name="second_name" class="form-control invalid" value="{{ isset($journalist) ? $journalist->second_name : old('second_name') }}">
        @if ( $errors->has('second_name') )
            <span class="help-block">{{ $errors->first('second_name') }}</span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        <label for="description">Description</label>
        <input type="textarea" name="description" class="form-control" value="{{ isset($journalist) ? $journalist->description : old('description') }}">
        @if ( $errors->has('description') )
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
        <label for="image">Image</label>
        <input type="file" id="image" name="image" value="{{ isset($journalist) ? $journalist->image : old('image') }}">
        @if ( $errors->has('image') )
            <span class="help-block">{{ $errors->first('image') }}</span>
        @endif
    </div>
</div>
