@extends('admin.layout')

@section('content')
<section class="content-header">
    <h3>Edit journalist</h3>
</section>
<section class="content">
<div class="row">
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit {{ $journalist->first_name }} {{ $journalist->second_name }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('admin.journalist.update', ['journalist' => $journalist->slug ], false )}}"
                  method="POST" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                @include('admin.journalist._form')

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                 </div>
            </form>

        </div>
    </div>
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Delete {{ $journalist->first_name }} {{ $journalist->second_name }}</h3>
            </div>
            <form  action="{{ route('admin.journalist.destroy',['journalist' => $journalist->slug] , False) }}"
                method="post">
                @method('DELETE')
                @csrf
                <div class="box-footer">
                    <input type="submit" name="Delete" value="Delete" class="btn btn-danger">
                </div>
            </form>
        </div>
    </div>
</div>
</section>
@endsection
