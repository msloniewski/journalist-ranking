@extends('admin.layout')

@section('content')
<section class="content-header">
    <h3>Create journalist</h3>
</section>
<section class="content">
<div class="row">
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Add journalist</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('admin.journalist.index', [], false) }}"
                  method="POST" enctype="multipart/form-data">
                @csrf
                @include('admin.journalist._form')
                  <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                 </div>
            </form>
        </div>
    </div>
</div>
</section>
@endsection
