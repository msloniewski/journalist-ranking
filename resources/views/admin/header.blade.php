<header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Ad.</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">

      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <!-- Navbar Right Menu -->

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <form id="logout-form" action="http://localhost:8000/logout" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                    <i class="fa fa-sign-out fa-2x"></i>
                </a>
            </li>
        </ul>
    </nav>
  </header>
