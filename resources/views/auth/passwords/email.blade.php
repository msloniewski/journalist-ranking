@extends('auth.layout')

@section('title')
    Password reset
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('journalist-list') }}"><b>Journalist Ranking</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Provide email to reset password</p>

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Send email</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
